<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/test', function(){
//   return 'Test API Success';
// });

Route::prefix('/data')->group(function () {
  Route::get('/get-data-header','Api\ApiDataController@getDataHeader');
  Route::get('/get-data-item','Api\ApiDataController@getDataItem');
  Route::get('/get-data-item-detail/{id}','Api\ApiDataController@getDataItemDetail');
  Route::get('/get-data-headers/{id}','Api\ApiDataController@getDataHeaderId');
});

Route::prefix('/post')->group(function () {
  Route::post('/post-data','Api\ApiPostController@PostData');
  Route::post('/update-data','Api\ApiPostController@UpdateData');
  Route::post('/delete-data','Api\ApiPostController@DeleteData');
});


Route::prefix('/data-item')->group(function () {
  Route::get('/get-data-item', 'Api\ItemDataController@GetDataItem');
});

Route::prefix('/post-item')->group(function () {
  Route::post('/post-data','Api\ItemPostController@PostData');
  Route::post('/update-data','Api\ItemPostController@UpdateData');
  Route::post('/delete-data','Api\ItemPostController@DeleteData');
});



Route::fallback(function() {
  $response = ['status' => false, 'message' => 'API yang anda cari tidak di temukan'];
  $json = json_encode($response);
  return $json;
});

