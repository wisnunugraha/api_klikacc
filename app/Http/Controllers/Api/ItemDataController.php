<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;
use DB;
use Str;


class ItemDataController extends Controller
{
  
  public function GetDataItem()
  {
    $result = array();
    $data = DB::table('ms_item')->get();
    if(empty($data)){ $response = ['status'=>false,'message'=>'Data Kosong', 'kode' => '01']; return $response; }
    else
    {
      $i =1;
      foreach ($data as $row) 
      {
        $column['id'] = $row->id;
        $column['no'] = $i++;
        $column['name'] = $row->name;
        $column['price'] = $row->price;
        $column['cost'] = $row->cost;
        $result[] = $column;
      }
      $response = ['status'=>true,'message'=>'Data Berhasil','Data' =>$result, 'kode' => '00']; return $response; 

    }

  }
}
