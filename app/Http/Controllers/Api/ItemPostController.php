<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;
use DB;
use Str;

class ItemPostController extends Controller
{
  public function PostData(Request $request)
  {

    DB::beginTransaction();

    try {
        DB::table('ms_item')
          ->insert([
            'name' => $request->item_name,
            'price' => $request->item_price,
            'cost' => $request->item_cost,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          ]);
        DB::commit();
        $response = ['status'=>true,'message'=>'Data Success Save', 'kode' => '00']; 
        return $response;
        // all good
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        $response = ['status'=>false,'message'=>'Data Error Save','read' => $e, 'kode' => '01']; 
        return $response;
    }

  }

  public function UpdateData(Request $request)
  {
    if($request->id)
    {
      DB::table('ms_item')
        ->where('id', $request->id)
        ->update([
          'name' => $request->name,
          'price' => $request->price,
          'cost' => $request->cost,
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        $response = ['status'=>true,'message'=>'Data Success Update', 'kode' => '00']; 
        return $response;

    }
    $response = ['status'=>false,'message'=>'Data Error Update', 'kode' => '01']; 
    return $response;
  }

  public function DeleteData(Request $request)
  {
    if($request->id)
    {
      DB::table('ms_item')
        ->where('id', $request->id)
        ->delete();
    $response = ['status'=>true,'message'=>'Data Success delete', 'kode' => '00']; 
    return $response;

    }
    $response = ['status'=>false,'message'=>'Data Error delete', 'kode' => '01']; 
    return $response;
  }
}
