<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;
use DB;
use Str;

class ApiPostController extends Controller
{
  /**
   * Dev - Wisnu Nugraha 
   */

  /**
   * Status Kode API
   * 00 - True Data
   * 01 - False Data -> Empty
   * 03 - Error Data -> Error Get
   */

  public function PostData(Request $request)
  {
      $count = DB::table('trx_po_h')->count();
      $count +=1;
      $po_number = Carbon::now()->format('Ymd').'-'.sprintf('%04s', $count);  

      DB::beginTransaction();

      try {
        
      $id_h = DB::table('trx_po_h')
                ->insertGetId([
                  'po_number' => $po_number,
                  'po_date' => Carbon::now()->format('Y-m-d'),
                  'po_price_total' => $request->total_p,
                  'po_cost_total' => $request->total_c,
                  'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
      DB::table('trx_po_d')
        ->insert([
          'po_h_id' => $id_h,
          'po_item_id' => $request->item_id,
          'po_item_qty' => $request->item_q,
          'po_item_price' => $request->item_p,
          'po_item_cost' => $request->item_c,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
      
          DB::commit();
          // all good
          $response = ['status'=>true,'message'=>'Data Success Save', 'kode' => '00']; 
          return $response;
      } catch (\Exception $e) {
          DB::rollback();
          // something went wrong
          $response = ['status'=>false,'message'=>'Data Error Save','read' => $e, 'kode' => '01']; 
          return $response;
      }

  }

  public function UpdateData(Request $request)
  {

    $id = DB::table('trx_po_h')->where('po_number','=', $request->po_number)->select('id')->first();
    
    DB::beginTransaction();

    try {
      
    DB::table('trx_po_h')
      ->where('id', $id->id)
      ->update([
        'po_price_total' => $request->total_p,
        'po_cost_total' => $request->total_c,
        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),        

      ]);
    DB::table('trx_po_d')
      ->where('po_h_id', $id->id)
      ->update([
        'po_item_id' => $request->item_id,
        'po_item_qty' => $request->item_q,
        'po_item_price' => $request->item_p,
        'po_item_cost' => $request->item_c,
        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),    
      ]);
    
        DB::commit();
        // all good
        $response = ['status'=>true,'message'=>'Data Success Save', 'kode' => '00']; 
        return $response;
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        $response = ['status'=>false,'message'=>'Data Error Save','read' => $e, 'kode' => '01']; 
        return $response;
    }

  }

  public function DeleteData(Request $request)
  {
    if($request->po_number)
    {
      $id = DB::table('trx_po_h')->where('po_number','=', $request->po_number)->select('id')->first();
      DB::table('trx_po_h')
        ->where('id', $id->id)
        ->delete();
      
      DB::table('trx_po_d')
        ->where('po_h_id', $id->id)
        ->delete();
      $response = ['status'=>true,'message'=>'Data Success Delete', 'kode' => '00']; 
      return $response;   
    }
    
    $response = ['status'=>false,'message'=>'Data Error Delete', 'kode' => '01']; 
    return $response;
  }
}
