<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;
use DB;
use Str;

class ApiDataController extends Controller
{
  /**
   * Dev - Wisnu Nugraha 
   */

  /**
   * Status Kode API
   * 00 - True Data
   * 01 - False Data -> Empty
   * 02 - Error Data -> Error Get
   */

  // public function getDataCount()
  // {
  //   $count = DB::table('trx_po_h')->count();
  //   if($count == 0) 
  //   {
  //     $count +=1;
  //     return Carbon::now()->format('Ymd').'-'.sprintf('%04s', $count);   
  //   }   
  //   return Carbon::now()->format('Ymd').'-'.sprintf('%04s', $count);   
  // }

  public function getDataItem()
  {
    $result = array();
    $data = DB::table('ms_item')->select(['id','name'])->get();

    if($data)
    {
      return $data;
    }
    else
    { $response = ['status'=>false,'message'=>'Data Kosong', 'kode' => '01']; return $response; }
  }
  public function getDataItemDetail($id)
  {
    // return $id;
    $data = DB::table('ms_item')->where('id',$id)->select(['price','cost'])->first();

    if($data)
    {
      $response = ['status'=>true,'message'=>'Data berhasil','data'=>$data, 'kode' => '01']; 
      return $response;
    }
    else
    { 
      $response = ['status'=>false,'message'=>'Data Kosong', 'kode' => '01']; 
      return $response; 
    }
  }

  public function getDataHeader()
  {
    $result = array();
    $data =  DB::table('trx_po_h')->get();

    if(empty($data)){ $response = ['status'=>false,'message'=>'Data Kosong', 'kode' => '01']; return $response; }
    else
    {
      $i =1;
      foreach ($data as $row)
      {
        $column['idHeader'] = $row->id;
        $column['no'] = $i++;
        $column['seriNumber'] = $row->po_number;
        $column['dateNumber'] = $row->po_date;
        $column['priceTotal'] = $row->po_price_total;
        $column['costTotal'] = $row->po_cost_total;
        $result[] = $column;
      }
      $response = ['status'=>true,'message'=>'Data Berhasil','Data' =>$result, 'kode' => '00']; return $response; 

    }

    


  $response = ['status'=>false,'message'=>'Data Error Network', 'kode' => '02']; 
  return $response;

  }

  public function getDataHeaderId($id)
  {
    $data = DB::table('trx_po_d')
              ->where('po_h_id', $id)
              ->first();
              
    if($data)
    {
      $response = ['status'=>true,'message'=>'Data berhasil','data'=>$data, 'kode' => '01']; 
      return $response;
    }
    else
    { 
      $response = ['status'=>false,'message'=>'Data Kosong', 'kode' => '01']; 
      return $response; 
    }
  }

}
