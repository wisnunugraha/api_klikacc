<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>API Klik ACC</title>
  </head>
  <body>

    <div class="container mt-5">
    <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-link " href="/" >Data</a>
        <a class="nav-link active" href="/item" >MS Item</a>
      </div>
    </nav>
    
    <div class="tab-content mt-2" id="nav-tabContent">
      <div class="tab-pane fade  show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        
    <!-- Button trigger modal -->
    
    <button type="button" class="btn btn-primary" id='btnAddNew' data-toggle="modal" data-target="#addModalNew">
      Add Item
    </button>
        <table class="table mt-5" id="table">
          <thead>
            <tr>
              <th scope="col">id</th>
              <th scope="col">No</th>
              <th scope="col">Name Item</th>
              <th scope="col">Price Item</th>
              <th scope="col">Cost Item</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>

      </div>
    </div>




    
    <!-- Modal -->
    <div class="modal fade" id="addModalNew" tabindex="-1" role="dialog" aria-labelledby="addModalNewLabel" aria-hidden="true">
      <div class="modal-dialog " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addModalNewLabel">Modal Add Item</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">                  
                  <label for="">Name Item</label>
                  <input type="text" class="form-control" id="val-item-name" placeholder="Name Item">
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-group">                  
                  <label for="">Price Item</label>
                  <input type="text" class="form-control" id="val-item-price" placeholder="Name Price">
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-group">                  
                  <label for="">Cost Item</label>
                  <input type="text" class="form-control" id="val-item-cost" placeholder="Name Cost">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="btnSaveData" class="btn btn-primary">Save Data</button>
          </div>
        </div>
      </div>
    </div>

    
    <!-- Modal -->
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
      <div class="modal-dialog " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="updateModalLabel">Modal Add Item</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" id="idUpdate">
                <div class="form-group">                  
                  <label for="">Name Item</label>
                  <input type="text" class="form-control" id="update-item-name" placeholder="Name Item">
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-group">                  
                  <label for="">Price Item</label>
                  <input type="text" class="form-control" id="update-item-price" placeholder="Name Price">
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-group">                  
                  <label for="">Cost Item</label>
                  <input type="text" class="form-control" id="update-item-cost" placeholder="Name Cost">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="btnUpdateData" class="btn btn-primary">Save Data</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">Modal Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" id="idDelete">
                <div class="form-group">                  
                  <label for="">Name Item</label>
                  <input type="text" class="form-control" id="delete-item-name" readonly placeholder="Name Item">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="btnDeleteData" class="btn btn-danger">Save Data</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->


    <script>
      $(document).ready(function() {
        (function($) {
          $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
              if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
              } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
              } else {
                this.value = "";
              }
            });
          };
        }(jQuery));
        
        var table = $('#table').DataTable({

                        "processing": true,
                        "bDestroy": true,
                        "bInfo": true,
                        "bSortable": false,
                        "bPaginate": true,
                        "bLengthChange": false,
                        "retrieve": true, 
                        // "bFilter": false, 
                          "order":[1,"asc"],

                        ajax: {
                          url: '/api/data-item/get-data-item',
                          type: 'GET',
                          dataSrc: 'Data',
                        },

                        'columns': [
                          {'data' : 'id'},
                          {'data' : 'no'},
                          {'data' : 'name'},
                          {'data' : 'price'},
                          {'data' : 'cost'},
                        ],

                        'columnDefs' : [
                          {
                            "targets": 0,
                            "visible": false,
                            // "width": "20%" ,
                          },
                          {
                            "targets": 5,
                            class:"text-center",
                            data: 'id',
                            render:function(data,type,row,meta){
                                return '<button class="btn btn-success waves-effect waves-light " id="btnUpdate" data-toggle="modal" data-target="#updateModal" > Update</button> '+
                                        ' <button class="btn btn-danger waves-effect waves-light " id="btnDelete" data-toggle="modal" data-target="#deleteModal" > Delete</button>';;
                            }
                          },

                        ],



                    });
        
        // $('#').val('')
        // $('#').val('')
        // $('#').val('')
        $("#val-item-price").inputFilter(function(value) {
          return /^\d*$/.test(value);    // Allow digits only, using a RegExp
        });
        $("#val-item-cost").inputFilter(function(value) {
          return /^\d*$/.test(value);    // Allow digits only, using a RegExp
        });

        $('#btnAddNew').on('click', function(){
          $('#val-item-name').val('')
          $('#val-item-price').val('')
          $('#val-item-cost').val('')
        });

        $('#btnSaveData').on('click', function() {
          var item_name =$('#val-item-name').val()
          var item_price =$('#val-item-price').val()
          var item_cost =$('#val-item-cost').val()

          if(item_name == '' && item_price == '' && item_cost == '')
          {
            alert('Please enter filled');
          }
          $.ajax({
            url : '/api/post-item/post-data',
            type : 'POST',
            method : 'POST',
            data : {
              'item_name': item_name,
              'item_price': item_price,
              'item_cost': item_cost,
            },
            success : function(result)
            {
              console.log(result);
              if(result.kode == '00')
              {
                $('#addModalNew').modal('hide');
                table.ajax.reload();
              }
              else
              {
                $('#addModalNew').modal('hide');
                table.ajax.reload();
                alert(result.message);

              }              

            }
          })

        })


        $('#table tbody').on('click','#btnUpdate', function(){
          $('#update-item-name').val('')
          $('#update-item-price').val('')
          $('#update-item-cost').val('')

          $('#idUpdate').val('')
          const data = table.row($(this).parents('tr')).data();
          $("#update-item-price").inputFilter(function(value) {
            return /^\d*$/.test(value);    // Allow digits only, using a RegExp
          });
          $("#update-item-cost").inputFilter(function(value) {
            return /^\d*$/.test(value);    // Allow digits only, using a RegExp
          });
          $('#idUpdate').val(data.id)
          $('#update-item-name').val(data.name)
          $('#update-item-price').val(data.price)
          $('#update-item-cost').val(data.cost)

        })

        $('#btnUpdateData').on('click', function(){
          var id = $('#idUpdate').val()
          var name =$('#update-item-name').val()
          var price =$('#update-item-price').val()
          var cost =$('#update-item-cost').val()

          if(name == '' && price == '' && cost == '')
          {
            alert('Please enter filled');
          }

          $.ajax({
            url: '/api/post-item/update-data',
            type: 'POST',
            method: 'POST',
            data : {
              'id': id,
              'name' : name,
              'price' : price,
              'cost' : cost,
            },
            success: function(response) 
            {

              if(response.kode == '00')
              {
                $('#updateModal').modal('hide');
                table.ajax.reload();
                alert(response.message)
              }
              else
              {
                $('#updateModal').modal('hide');
                table.ajax.reload();
                alert(response.message);
              }

            }
          })


        })

        

        $('#table tbody').on('click','#btnDelete', function(){
          $('#idDelete').val('')
          $('#delete-item-name').val('')
          const data = table.row($(this).parents('tr')).data();
          $('#idDelete').val(data.id)
          $('#delete-item-name').val(data.name)

        })

        
        $('#btnDeleteData').on('click', function(){
          var id = $('#idDelete').val()
          var name =$('#delete-item-name').val()

          if(name == ''  && id == '')
          {
            alert('Please enter filled');
          }

          $.ajax({
            url: '/api/post-item/delete-data',
            type: 'POST',
            method: 'POST',
            data : {
              'id': id,
            },
            success: function(response) 
            {

              if(response.kode == '00')
              {
                $('#deleteModal').modal('hide');
                table.ajax.reload();
                alert(response.message)
              }
              else
              {
                $('#deleteModal').modal('hide');
                table.ajax.reload();
                alert(response.message);
              }

            }
          })


        })

       


        
      });
    </script>



  </body>
</html>