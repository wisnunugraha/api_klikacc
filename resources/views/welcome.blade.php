<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>API Klik ACC</title>
  </head>
  <body>

    <div class="container mt-5">
    <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-link active" href="/" >Data</a>
        <a class="nav-link" href="/item">MS Item</a>
      </div>
    </nav>
    
    <div class="tab-content mt-2" id="nav-tabContent">
      <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" id='btnAddData' data-toggle="modal" data-target="#addModal">
      Add Data
    </button>
        <table class="table mt-5" id="table-data">
          <thead>
            <tr>
              <th scope="col">id_header</th>
              <th scope="col">No</th>
              <th scope="col">Seri Number</th>
              <th scope="col">Date</th>
              <th scope="col">Price Total</th>
              <th scope="col">Cost Total</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        
      </div>
      <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        

      </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addModalLabel">Modal Add New</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                  <label for="">PO Price Total</label>
                  <input type="text" class="form-control" id="val-add-po-price-total" readonly aria-describedby="" placeholder="PO Price Total">
                </div>

              </div>
              <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                  <label for="">PO Cost Total</label>
                  <input type="text" class="form-control" id="val-add-po-cost-total" readonly aria-describedby="" placeholder="PO Cost Total">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-sm-6">
                <div class="form-group">
                  <label for="">Kode PO Cost Total</label>
                  <select name="" class="form-control" id="val-data-item">
                    <option value="">Pilih Data Item</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                <div class="form-group">
                  <label for="">Price Item</label>
                  <input type="text" class="form-control" id="val-data-price" readonly aria-describedby="" placeholder="Price Item">
                </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                <div class="form-group">
                  <label for="">Cost Item</label>
                  <input type="text" class="form-control" id="val-data-cost" readonly aria-describedby="" placeholder="Cost Item">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label for="">Jumlah Quantity</label>
                  <input type="text" class="form-control" id="val-data-quantity" aria-describedby="" placeholder="Price Item">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="btnSaveData" class="btn btn-primary">Save Data</button>
          </div>
        </div>
      </div>
    </div>

    
    <!-- Modal -->
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="updateModalLabel">Modal Update</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">PO Serial Number</label>
                  <input type="text" class="form-control" id="update-add-po-number" readonly aria-describedby="" placeholder="PO Number">
                </div>                
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">PO Date</label>
                  <input type="date" class="form-control" id="update-add-po-date" readonly aria-describedby="" placeholder="PO Date">
                </div>                
              </div>
              <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                  <label for="">PO Price Total</label>
                  <input type="text" class="form-control" id="update-add-po-price-total" readonly aria-describedby="" placeholder="PO Price Total">
                </div>
              </div>
              <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                  <label for="">PO Cost Total</label>
                  <input type="text" class="form-control" id="update-add-po-cost-total" readonly aria-describedby="" placeholder="PO Cost Total">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-sm-6">
                <div class="form-group">
                  <label for="">Kode PO Cost Total</label>
                  <select name="" class="form-control" id="update-data-item">
                    <option value="">Pilih Data Item</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                <div class="form-group">
                  <label for="">Price Item</label>
                  <input type="text" class="form-control" id="update-data-price" readonly aria-describedby="" placeholder="Price Item">
                </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                <div class="form-group">
                  <label for="">Cost Item</label>
                  <input type="text" class="form-control" id="update-data-cost" readonly aria-describedby="" placeholder="Cost Item">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label for="">Jumlah Quantity</label>
                  <input type="text" class="form-control" id="update-data-quantity" aria-describedby="" placeholder="Price Item">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="btnUpdateData" class="btn btn-primary">Save Data</button>
          </div>
        </div>
      </div>
    </div>

    
    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">Modal Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">PO Serial Number</label>
                  <input type="text" class="form-control" id="delete-add-po-number" readonly aria-describedby="" placeholder="PO Number">
                </div>                
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">PO Date</label>
                  <input type="date" class="form-control" id="delete-add-po-date" readonly aria-describedby="" placeholder="PO Date">
                </div>                
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="btnDeleteData" class="btn btn-danger">Delete Data</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->


    <script>
      $(document).ready(function() {
        (function($) {
          $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
              if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
              } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
              } else {
                this.value = "";
              }
            });
          };
        }(jQuery));
        
        var table = $('#table-data').DataTable({

                        "processing": true,
                        "bDestroy": true,
                        "bInfo": true,
                        "bSortable": false,
                        "bPaginate": true,
                        "bLengthChange": false,
                        "retrieve": true, 
                        // "bFilter": false, 
                          "order":[1,"asc"],

                        ajax: {
                          url: '/api/data/get-data-header',
                          type: 'GET',
                          dataSrc: 'Data',
                        },

                        'columns': [
                          {'data' : 'idHeader'},
                          {'data' : 'no'},
                          {'data' : 'seriNumber'},
                          {'data' : 'dateNumber'},
                          {'data' : 'priceTotal'},
                          {'data' : 'costTotal'},

                        ],

                        'columnDefs' : [
                          {
                            "targets": 0,
                            "visible": false,
                            // "width": "20%" ,
                          },
                          {
                            "targets": 6,
                            class:"text-center",
                            data: 'idHeader',
                            render:function(data,type,row,meta){
                                return '<button class="btn btn-success waves-effect waves-light " id="btnUpdate" data-toggle="modal" data-target="#updateModal" > Update</button> '+
                                        ' <button class="btn btn-danger waves-effect waves-light " id="btnDelete" data-toggle="modal" data-target="#deleteModal" > Delete</button>';;
                            }
                          },

                        ],



                    });
        
        // $('#').val('')
        // $('#').val('')
        // $('#').val('')
        // $('#val-data-quantity').val('')
        $("#val-data-quantity").inputFilter(function(value) {
          return /^\d*$/.test(value);    // Allow digits only, using a RegExp
        });
        $('#btnAddData').on('click', function() {
          $('#val-add-po-date').val('')
          $('#val-add-po-price-total').val('')
          $('#val-add-po-cost-total').val('')

          $('#val-data-item').val('')
          $('#val-data-price').val('')
          $('#val-data-cost').val('')
          $('#val-data-quantity').val('')
          $.ajax({
            url: '/api/data/get-data-item',
            method: 'get',
            type: 'GET',
            success: function(item)
            {
              $('#val-data-item').html($('<option>', {
                  value: '',
                  text:  'Pilih Data Item',
              }));
              $.each(item, function(index, value) {
                $('#val-data-item').append($('<option>', {
                    value: value.id,
                    text:  value.name,
                }));

              })
            }
          })

        }); 
        
        $('#val-data-item').on('change', function() {
          $('#val-data-price').val('')
          $('#val-data-cost').val('')
          $('#val-add-po-price-total').val('')
          $('#val-add-po-cost-total').val('')
          $('#val-data-quantity').val('')
          var id = $(this).val();
          $.ajax({
            url: '/api/data/get-data-item-detail/'+id,
            type: 'GET',
            success: function(detail)
            {
              var result = detail.data;
              $('#val-data-price').val(result.price)
              $('#val-data-cost').val(result.cost)

            }
          })
        });

        
        $('#val-data-quantity').on('focusin chage input', function() {
          var val = $(this).val();
          var price = $('#val-data-price').val();
          var cost = $('#val-data-cost').val();
          if(price == '' && cost == '')
          {
            $('#val-data-quantity').val('')
            alert('Data Price and Cost Empty')
          }

          var total_price = val*price;
          var total_cost = val*cost;
          $('#val-add-po-price-total').val(total_price)
          $('#val-add-po-cost-total').val(total_cost)
 
        });

        $('#btnSaveData').on('click', function() {
          var total_price = $('#val-add-po-price-total').val()
          var total_cost = $('#val-add-po-cost-total').val()

          if(total_price == '' && total_cost == '' || total_price == 0 && total_cost == 0)
          {
            alert('Data stil empty.');
          }
          var  total_p = $('#val-add-po-price-total').val()
          var  total_c = $('#val-add-po-cost-total').val()

          var  item_id = $('#val-data-item').val()
          var  item_p = $('#val-data-price').val()
          var  item_c = $('#val-data-cost').val()
          var  item_q = $('#val-data-quantity').val()

          $.ajax({
            url: '/api/post/post-data',
            type: 'POST',
            method: 'POST',
            data : {
              'total_p':total_p,
              'total_c':total_c,
              'item_id':item_id,
              'item_p':item_p,
              'item_c':item_c,
              'item_q':item_q,
            },
            success: function(kode)
            {
              if(kode.kode == '00')
              {
                $('#addModal').modal('hide');
                table.ajax.reload();
              }
              else
              {
                alert(kode.message);
                $('#addModal').modal('hide');
                table.ajax.reload();
              }

            }
          })

          
        })


        $('#table-data tbody').on('click','#btnUpdate', function(){
          $('#update-add-po-number').val('')
          $('#update-add-po-date').val('')
          $('#update-add-po-price-total').val('')
          $('#update-add-po-cost-total').val('')

          $('#update-data-item').val('')
          $.ajax({
            url: '/api/data/get-data-item',
            method: 'get',
            type: 'GET',
            success: function(item)
            {
              $('#update-data-item').html($('<option>', {
                  value: '',
                  text:  'Pilih Data Item',
              }));
              $.each(item, function(index, value) {
                $('#update-data-item').append($('<option>', {
                    value: value.id,
                    text:  value.name,
                }));

              })
            }
          })
          $('#update-data-price').val('')
          $('#update-data-cost').val('')
          $('#update-data-quantity').val('')
          const data = table.row($(this).parents('tr')).data();
          $('#update-add-po-number').val(data.seriNumber)
          $('#update-add-po-date').val(data.dateNumber)
          $('#update-add-po-price-total').val(data.priceTotal)
          $('#update-add-po-cost-total').val(data.costTotal)
          $.ajax({
            url: '/api/data/get-data-headers/'+data.idHeader,
            type: 'GET',
            method: 'GET',
            success:function(response)
            {
              console.log(response)
              var result = response.data
              $('#update-data-item').val(result.po_item_id);
              $('#update-data-price').val(result.po_item_price)
              $('#update-data-cost').val(result.po_item_cost)
              $('#update-data-quantity').val(result.po_item_qty)
            }
          })
        });
        
        $('#update-data-item').on('change', function() {
          $('#update-data-price').val('')
          $('#update-data-cost').val('')
          $('#update-add-po-price-total').val('')
          $('#update-add-po-cost-total').val('')
          $('#update-data-quantity').val('')
          var id = $(this).val();
          $.ajax({
            url: '/api/data/get-data-item-detail/'+id,
            type: 'GET',
            success: function(detail)
            {
              var result = detail.data;
              $('#update-data-price').val(result.price)
              $('#update-data-cost').val(result.cost)

            }
          })
        });
        
        $('#update-data-quantity').on('focusin chage input', function() {
          var val = $(this).val();
          var price = $('#update-data-price').val();
          var cost = $('#update-data-cost').val();
          if(price == '' && cost == '')
          {
            $('#update-data-quantity').val('')
            alert('Data Price and Cost Empty')
          }

          var total_price = val*price;
          var total_cost = val*cost;
          $('#update-add-po-price-total').val(total_price)
          $('#update-add-po-cost-total').val(total_cost)
 
        });

        
        $('#btnUpdateData').on('click', function() {
          var total_price = $('#update-add-po-price-total').val()
          var total_cost = $('#update-add-po-cost-total').val()

          if(total_price == '' && total_cost == '' || total_price == 0 && total_cost == 0)
          {
            alert('Data stil empty.');
          }
          var  po_number = $('#update-add-po-number').val();
          var  total_p = $('#update-add-po-price-total').val()
          var  total_c = $('#update-add-po-cost-total').val()

          var  item_id = $('#update-data-item').val()
          var  item_p = $('#update-data-price').val()
          var  item_c = $('#update-data-cost').val()
          var  item_q = $('#update-data-quantity').val()

          $.ajax({
            url: '/api/post/update-data',
            type: 'POST',
            method: 'POST',
            data : {
              'po_number':po_number,
              'total_p':total_p,
              'total_c':total_c,
              'item_id':item_id,
              'item_p':item_p,
              'item_c':item_c,
              'item_q':item_q,
            },
            success: function(kode)
            {
              if(kode.kode == '00')
              {
                $('#updateModal').modal('hide');
                table.ajax.reload();
              }
              else
              {
                alert(kode.message);
                $('#updateModal').modal('hide');
                table.ajax.reload();
              }

            }
          })

          
        })


        
        $('#table-data tbody').on('click','#btnDelete', function(){
          $('#delete-add-po-number').val('')
          $('#delete-add-po-date').val('')
          const data = table.row($(this).parents('tr')).data();
          $('#delete-add-po-number').val(data.seriNumber)
          $('#delete-add-po-date').val(data.dateNumber)
        });

        $('#btnDeleteData').on('click', function() {
          var po_number =$('#delete-add-po-number').val()
          var po_date =$('#delete-add-po-date').val()
          
          $.ajax({
            url: '/api/post/delete-data',
            type: 'POST',
            method: 'POST',
            data : {
              'po_number':po_number,
              'po_date':po_date,
            },
            success: function(kode)
            {
              if(kode.kode == '00')
              {
                $('#deleteModal').modal('hide');
                table.ajax.reload();
              }
              else
              {
                alert(kode.message);
                $('#deleteModal').modal('hide');
                table.ajax.reload();
              }

            }
          })

        });

        





        
      });
    </script>
    
  </body>
</html>